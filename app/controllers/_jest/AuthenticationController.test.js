const AuthenticationController = require('../AuthenticationController');
const { User } = require('../../models');
const { InsufficientAccessError } = require('../../errors');
const bcrypt = require('bcrypt');
const jsonwebtoken = require('jsonwebtoken');

describe('AuthenticationController', () => {
  describe('#authorize', () => {
    it('it should be athorize', async () => {
      const mockRequest = {
        token: {
          header: {
            authorize: () => {
              return;
            },
          },
        },
        payload: jest.fn().mockReturnThis(),
      };
      const user = new User(mockRequest.token, mockRequest.payload);
      const mockResponse = {
        status: jest.fn().mockReturnThis(),
        json: jest.fn().mockReturnThis(),
      };
      const err = new InsufficientAccessError();
      const error = {
        error: {
          name: err.name,
          message: err.message,
          details: err.details || null,
        },
      };
      const authenticationController = new AuthenticationController('userModel', 'roleModel', 'bcrypt', 'jwt');
      const result = authenticationController.authorize(mockRequest, mockResponse);
      //   expect(result.user).toStrictEqual(user);
      //   expect(result.status).toEqual(401);
      //   expect(result.json).toBe(error);
    });
  });

  describe('#handleLogin', () => {
    it('it should handle login', async () => {});
  });
});
